﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using GameTools;
using MyFirstProgram;

namespace MatrixHugo
{
    class Matrix : GameEngine
    {
        MatrixRepresentation mR;
        //PrintColorines pC;
        char[] letras;
        int lletra;
        int column;
        bool puedeBajar;

        protected override void Start()
        {
            mR = new MatrixRepresentation(20, 40);
            mR.CleanTheMatrix();
            base.Start();
        }

        protected override void Update()
        {
            letras = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            var rnd = new Random();
            var pr = new Colores();
            
            column = rnd.Next(0, mR.TheMatrix.GetLength(1));
            lletra = rnd.Next(0, letras.Length-1);

            mR.TheMatrix[0, column] = letras[lletra];

            for (int y = 0; y < mR.TheMatrix.GetLength(1); y++)
            {
                for (int x = 0; x < mR.TheMatrix.GetLength(0)-1; x++)
                {
                    if (mR.TheMatrix[19, y] != '0')
                    {
                        mR.TheMatrix[19, y] = '0';
                    }
                    if (mR.TheMatrix[x, y] != '0')
                    {
                        if (y != mR.TheMatrix.GetLength(0) && y != 0)
                        {
                            if (puedeBajar)
                            {
                                mR.TheMatrix[x + 1, y] = mR.TheMatrix[x, y];
                                mR.TheMatrix[x, y] = '0';
                                puedeBajar = false;
                            }
                        }
                        else
                        {
                            mR.TheMatrix[x, y] = '0';
                        }
                    }

                }
                puedeBajar = true;
            }

            //this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;

            //mR.printMatrix(mR.TheMatrix);
            pr.printMatrix(mR.TheMatrix);

            base.Update();
        }

        protected override void Exit()
        {
            //Code afer last frame
            base.Exit();
        }

    }
}
