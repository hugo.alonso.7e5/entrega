﻿using MyFirstProgram;
using System;

namespace MatrixHugo
{
    class Colores : MatrixRepresentation
    {
        public  void printMatrix(char[,] _theMatrix)
        {
            Console.Clear();
            for (int x = 0; x < _theMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < _theMatrix.GetLength(1); y++)
                {
                    //Console.Write($" {_theMatrix[x, y]}");
                    //Hemos creado el if para representar el cambio de colores
                    //en la matriz (solo es verde la letra, el resto no se ve)
                    if (_theMatrix[x, y] != '0')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" {_theMatrix[x, y]}");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write($" {_theMatrix[x, y]}");
                        Console.ResetColor();

                    }
                }
                Console.WriteLine();
            }
        }

    }
}
